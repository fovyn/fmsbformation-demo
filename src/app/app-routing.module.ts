import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PokemonModule} from './modules/pokemon/pokemon.module';


const routes: Routes = [
  { path: 'pokemon', loadChildren: () => PokemonModule }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

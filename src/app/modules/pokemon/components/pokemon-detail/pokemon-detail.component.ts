import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {PokemonService} from '../../services/pokemon.service';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit, OnChanges {
  @Input() url: string;
  region: any;

  constructor(private pokemonService: PokemonService) { }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.url && changes.url.currentValue != null) {
      this.pokemonService.getDetail(this.url).subscribe(data => this.region = data);
    }
  }

}

import { Component, OnInit } from '@angular/core';
import {PokemonService} from '../../services/pokemon.service';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
  regions: Observable<any>;
  currentUrl: string;

  constructor(private pokemonService: PokemonService, private router: Router) { }

  ngOnInit() {
    this.regions = this.pokemonService.pokedexes;
  }

  goToDetail(pokedex: {name: string, url: string}) {
    this.currentUrl = pokedex.url;
  }
}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(private httpClient: HttpClient) { }

  get pokedexes(): Observable<any> {
    return this.httpClient
      .get<any>('https://pokeapi.co/api/v2/pokedex')
      .pipe(map(data => data.results));
  }

  getDetail(url: string): Observable<any> {
    return this.httpClient
      .get<any>(url)
      .pipe(map(data => {
        return {name: data.name, entries: data.pokemon_entries};
      }));
  }
}

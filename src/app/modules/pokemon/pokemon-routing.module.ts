import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PokemonComponent} from './components/pokemon/pokemon.component';
import {PokemonListComponent} from './components/pokemon-list/pokemon-list.component';


const routes: Routes = [
  { path: '', component: PokemonComponent, children: [
      { path: 'list', component: PokemonListComponent }
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PokemonRoutingModule { }

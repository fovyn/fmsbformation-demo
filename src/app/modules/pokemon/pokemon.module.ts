import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokemonRoutingModule } from './pokemon-routing.module';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import {HttpClientModule} from '@angular/common/http';
import {PokemonService} from './services/pokemon.service';
import { PokemonDetailComponent } from './components/pokemon-detail/pokemon-detail.component';
import { BgColorRedDirective } from './directives/bg-color-red.directive';
import { MCollapsibleDirective } from './directives/m-collapsible.directive';


@NgModule({
  declarations: [PokemonComponent, PokemonListComponent, PokemonDetailComponent, BgColorRedDirective, MCollapsibleDirective],
  imports: [
    CommonModule,
    PokemonRoutingModule,
    HttpClientModule,
  ],
  providers: [PokemonService]
})
export class PokemonModule { }

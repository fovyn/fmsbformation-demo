import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[bgColorRed]'
})
export class BgColorRedDirective {

  constructor(private el: ElementRef<HTMLDivElement>) {
    this.el.nativeElement.style.backgroundColor = '#FF0000';
  }

}

import {Directive, ElementRef, EventEmitter, Output} from '@angular/core';

declare const M: any;

@Directive({
  selector: '[mCollapsible]'
})
export class MCollapsibleDirective {
  mInstance: any;
  @Output('mInstance') mInstanceEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(private el: ElementRef<HTMLElement>) {
    this.mInstance = M.Collapsible.init(el.nativeElement);
    this.mInstanceEvent.emit(this.mInstance);
  }


}

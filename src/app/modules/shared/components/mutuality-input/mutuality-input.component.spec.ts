import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MutualityInputComponent } from './mutuality-input.component';

describe('MutualityInputComponent', () => {
  let component: MutualityInputComponent;
  let fixture: ComponentFixture<MutualityInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MutualityInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MutualityInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mutuality-input',
  templateUrl: './mutuality-input.component.html',
  styleUrls: ['./mutuality-input.component.scss']
})
export class MutualityInputComponent implements OnInit {
  @Input() key: string;
  @Input() keyLabel: string;
  @Input() formControl: FormControl;
  @Input() type: 'text' | 'number';

  constructor() { }

  ngOnInit() {
  }

}

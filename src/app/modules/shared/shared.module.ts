import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MutualityInputComponent } from './components/mutuality-input/mutuality-input.component';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [MutualityInputComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [MutualityInputComponent]
})
export class SharedModule { }
